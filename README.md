# IPL PROJECT - 1

In this data assignment you will transform raw data of IPL to calculate the following stats:

1. Number of matches played per year for all the years in IPL.
2. Number of matches won of per team per year in IPL.
3. Extra runs conceded per team in 2016
4. Top 10 economical bowlers in 2015

Implement the 4 functions, one for each task.
Use the functions results to dump json files in the output folder

### EXTRA DELIVERABLES

More questions in IPL project for practice:

- (Find the number of times each team won the toss and also won the match)
- (Find player per season who has won the highest number of _Player of the Match_ awards)
- Find the strike rate of the batsman Virat Kohli for each season
- Find the highest number of times one player has been dismissed by another player
- Find the bowler with the best economy in super overs

---

# IPL PROJECT - 2

1. HTTP Server to serve different static files at different endpoints.
2. Serve one single html file.
3. Use HighCharts to render a chart for each of the questions you had solved

CHECKLISTS

    a) Must use the http routes you created for each of the solutions you had built
    b) Appropriate status codes for routes
    c) One html file for each function
    d) Each html file must have atleast all the minimum needed HTML tags
    e) One index.html file that links to the html file for each of the functions
    f) Bonus: Frontend javascript in a separate file

---

# IPL PROJECT - 3
1. Connect to postgres via node ( Use ENV variables, DO NOT CONNECT USING root)
2. Import your input JSON data to postgres using a dedicated NodeJS function.
3. Preserving the frontend functionality, create API routes to serve the data to the frontend.

CHECKLISTS

    a) Directory structure
    b) Git commits to be detailed
    c) package.json - dependencies, devDependencies, scripts
    d) .gitignore file
    e) Variable names, function names, file names, etc should be self describing
    f) Separate module for functions
    g) Functional programming - map, reduce, filter, sort
    h) Error handling with nice frontend messages
    i) Function documentation detailing the parameters being used, return values with samples
    j) Routes to be named appropriately
    k) Redirect your backend STDOUT to a file and name it server.log
    l) Promises and/or callbacks to be used only
