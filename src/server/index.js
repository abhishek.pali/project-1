// using dotenv module to load environment variables
require('dotenv').config();

// exporting functions from ipl.js
const ipl = require("./ipl.js");

// loading file system modules
var fs = require("fs");

// creating a client to interact with postgres database
const { Client } = require("pg");
const client = new Client();

client.connect(function(err){
  if(err)
  {
      console.log(`Error occurred while connecting \n${err}`);
  }
});

// promise to make queries to the database and send the results to
// different functions to calculate different ipl stats
var promise = new Promise(function(resolve, reject) {
  client.query('SELECT * FROM matches', (err, res) => {
    if (err) {
      console.log(`There is something wrong with matches data \n${err}`);
    } else {
      client.query('SELECT * FROM deliveries', (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve({
            matchesjson: res.rows,
            deliveriesjson: result.rows
          });
        }
      });
    }
  });  
})

promise.then((obj) => {

    // storing the result of matchesPerYear function to a matchesPerYearSolution
    // variable
    var matchesPerYearSolution = ipl.matchesPerYear(obj.matchesjson);
    // writing the value stored in matchesPerYearSolution variable to
    // matchesPerYear.json file with error handling
    fs.writeFile(
      "../output/matchesPerYear.json",
      JSON.stringify(matchesPerYearSolution),
      err => {
        if (err) throw err;
        console.log("matchesPerYear.json file has been saved!");
      }
    );

    // storing the result of extraRunsPerTeam function to a
    // extraRunsPerTeamSolution variable
    var extraRunsPerTeamSolution = ipl.extraRunsPerTeam2016(
      obj.matchesjson,
      obj.deliveriesjson,
      2016
    );
    // writing the value stored in extraRunsPerTeamSolution variable to
    // extraRunsPerTeam.json file with error handling
    fs.writeFile(
      "../output/extraRunsPerTeam.json",
      JSON.stringify(extraRunsPerTeamSolution),
      err => {
        if (err) throw err;
        console.log("extraRunsPerTeam.json file has been saved!");
      }
    );

    // storing the result of numOfMatchesWonPtPy function to a
    // numOfMatchesWonPtPySolution variable
    var numOfMatchesWonPtPySolution = ipl.numOfMatchesWonPtPy(obj.matchesjson);
    // writing the value stored in numOfMatchesWonPtPySolution variable to
    // numOfMatchesWonPtPy.json file with error handling
    fs.writeFile(
      "../output/numOfMatchesWonPtPy.json",
      JSON.stringify(numOfMatchesWonPtPySolution),
      err => {
        if (err) throw err;
        console.log("numOfMatchesWonPtPy.json file has been saved!");
      }
    );

    // storing the result of top10EconomicalBowlers function to a
    // top10EconomicalBowlersSolution variable
    var top10EconomicalBowlersSolution = ipl.top10EconomicalBowlers(
      obj.matchesjson,
      obj.deliveriesjson,
      2015
    );
    // writing the value stored in top10EconomicalBowlersSolution variable
    //  to top10EconomicalBowlers.json file with error handling
    fs.writeFile(
      "../output/top10EconomicalBowlers.json",
      JSON.stringify(top10EconomicalBowlersSolution),
      err => {
        if (err) throw err;
        console.log("top10EconomicalBowlers.json file has been saved!");
      }
    );

    // storing the result of tossWonAndMatchWon function to a
    // tossWonAndMatchWonSolution variable
    var tossWonAndMatchWonSolution = ipl.tossWonAndMatchWon(obj.matchesjson);
    // writing the value stored in tossWonAndMatchWonSolution variable
    // to tossWonAndMatchWon.json file with error handling
    fs.writeFile(
      "../output/tossWonAndMatchWon.json",
      JSON.stringify(tossWonAndMatchWonSolution),
      err => {
        if (err) throw err;
        console.log("tossWonAndMatchWon.json file has been saved!");
      }
    );

    // storing the result of playerOfTheSeason function to a
    // playerOfTheSeasonSolution variable
    var playerOfTheSeasonSolution = ipl.playerOfTheSeason(obj.matchesjson);
    // writing the value stored in playerOfTheSeasonSolution variable
    // to playerOfTheSeason.json file with error handling
    fs.writeFile(
      "../output/playerOfTheSeason.json",
      JSON.stringify(playerOfTheSeasonSolution),
      err => {
        if (err) throw err;
        console.log("playerOfTheSeason.json file has been saved!");
      }
    );

    // storing the result of strikeRateOfVirat function to a
    // strikeRateOfViratSolution variable
    var strikeRateOfViratSolution = ipl.strikeRateOfVirat(
      obj.matchesjson,
      obj.deliveriesjson
    );
    // writing the value stored in strikeRateOfViratSolution variable
    // to strikeRateOfVirat.json file with error handling
    fs.writeFile(
      "../output/strikeRateOfVirat.json",
      JSON.stringify(strikeRateOfViratSolution),
      err => {
        if (err) throw err;
        console.log("strikeRateOfVirat.json file has been saved!");
      }
    );

    // storing the result of economyOfBowlersInSuperOvers function
    // to a economyOfBowlersInSuperOversSolution variable
    var economyOfBowlersInSuperOversSolution = ipl.economyOfBowlersInSuperOvers(
      obj.deliveriesjson
    );
    // writing the value stored in economyOfBowlersInSuperOversSolution
    // variable to economyOfBowlersInSuperOvers.json file with error handling
    fs.writeFile(
      "../output/economyOfBowlersInSuperOvers.json",
      JSON.stringify(economyOfBowlersInSuperOversSolution),
      err => {
        if (err) throw err;
        console.log("economyOfBowlersInSuperOvers.json file has been saved!");
      }
    );

    // storing the result of playerDismissal function to a
    // playerDismissalSolution variable
    var playerDismissalSolution = ipl.playerDismissal(obj.deliveriesjson);
    // writing the value stored in playerDismissalSolution variable to
    // playerDismissal.json file with error handling
    fs.writeFile(
      "../output/playerDismissal.json",
      JSON.stringify(playerDismissalSolution),
      err => {
        if (err) throw err;
        console.log("playerDismissal.json file has been saved!");
      }
    );

}).catch((message) => {
  console.log(`Oops. Something went wrong with deliveries data \n${message}`);
});

// loading http modules
var http = require("http");

// creating a HTTP Server to serve different static files at different endpoints
var server = http.createServer(function(request, response) {
  // created a callback function for error handling
  var callback = function(err, data) {
    if (err) {
      response.end("404: Page could not be found");
      console.log(`Oops! Something went wrong \n ${err}`);
    } else {
      response.end(data, "utf-8");
    }
  };

  // switch case to look up to the url entered and display the output
  // included the status code and content type of the response
  switch (request.url) {
    case "/":
      response.writeHead(200, { "Content-Type": "text/html" });
      fs.readFile("./index.html", callback);
      break;
    case "/matchesPerYear":
      response.writeHead(200, { "Content-Type": "application/json" });
      fs.readFile("../output/matchesPerYear.json", callback);
      break;
    case "/matchesPerYear.html":
      response.writeHead(200, { "Content-Type": "text/html" });
      fs.readFile("./matchesPerYear.html", callback);
      break;
    case "/numOfMatchesWonPtPy":
      response.writeHead(200, { "Content-Type": "application/json" });
      fs.readFile("../output/numOfMatchesWonPtPy.json", callback);
      break;
    case "/numOfMatchesWonPtPy.html":
      response.writeHead(200, { "Content-Type": "text/html" });
      fs.readFile("./numOfMatchesWonPtPy.html", callback);
      break;
    case "/extraRunsPerTeam":
      response.writeHead(200, { "Content-Type": "application/json" });
      fs.readFile("../output/extraRunsPerTeam.json", callback);
      break;
    case "/extraRunsPerTeam.html":
      response.writeHead(200, { "Content-Type": "text/html" });
      fs.readFile("./extraRunsPerTeam.html", callback);
      break;
    case "/strikeRateOfVirat":
      response.writeHead(200, { "Content-Type": "application/json" });
      fs.readFile("../output/strikeRateOfVirat.json", callback);
      break;
    case "/strikeRateOfVirat.html":
      response.writeHead(200, { "Content-Type": "text/html" });
      fs.readFile("./strikeRateOfVirat.html", callback);
      break;
    case "/playerOfTheSeason":
      response.writeHead(200, { "Content-Type": "application/json" });
      fs.readFile("../output/playerOfTheSeason.json", callback);
      break;
    case "/playerOfTheSeason.html":
      response.writeHead(200, { "Content-Type": "text/html" });
      fs.readFile("./playerOfTheSeason.html", callback);
      break;
    case "/top10EconomicalBowlers":
      response.writeHead(200, { "Content-Type": "application/json" });
      fs.readFile("../output/top10EconomicalBowlers.json", callback);
      break;
    case "/top10EconomicalBowlers.html":
      response.writeHead(200, { "Content-Type": "text/html" });
      fs.readFile("./top10EconomicalBowlers.html", callback);
      break;
    case "/tossWonAndMatchWon":
      response.writeHead(200, { "Content-Type": "application/json" });
      fs.readFile("../output/tossWonAndMatchWon.json", callback);
      break;
    case "/tossWonAndMatchWon.html":
      response.writeHead(200, { "Content-Type": "text/html" });
      fs.readFile("./tossWonAndMatchWon.html", callback);
      break;
    case "/economyOfBowlersInSuperOvers":
      response.writeHead(200, { "Content-Type": "application/json" });
      fs.readFile("../output/economyOfBowlersInSuperOvers.json", callback);
      break;
    case "/economyOfBowlersInSuperOvers.html":
      response.writeHead(200, { "Content-Type": "text/html" });
      fs.readFile("./economyOfBowlersInSuperOvers.html", callback);
      break;
    case "/playerDismissal":
      response.writeHead(200, { "Content-Type": "application/json" });
      fs.readFile("../output/playerDismissal.json", callback);
      break;
    case "/playerDismissal.html":
      response.writeHead(200, { "Content-Type": "text/html" });
      fs.readFile("./playerDismissal.html", callback);
      break;
    default:
      response.writeHead(404, { "Content-Type": "text/html" });
      fs.readFile(request.url, callback);
      break;
  }
});

server.listen(8000);
console.log("listening to port 8000");