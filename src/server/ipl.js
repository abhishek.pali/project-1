module.exports = {
  //matchesPerYear function to calculate number of matches played per year

  matchesPerYear: function(matches) {
    /* 
      matchesPerYearObj = {
                            "2008": 59,
                            "2009": 70
                          }
    */
    let matchesPerYearObject = matches.reduce(function(yearObject, element) {

      if (element.season in yearObject) 
        {
          yearObject[element.season]++;
        } 
      else 
        {
          yearObject[element.season] = 1;
        }

      return yearObject;
    }, {});

    return matchesPerYearObject;
  },

  // numOfMatchesWonPtPy function to calculate number of matches won per team per year

  numOfMatchesWonPtPy: function(matches) {

    let yearArr = matches.reduce(function(arr, element) {
      if (arr.indexOf(element.season) === -1) 
        {
          arr.push(element.season);
        }
      return arr;
    }, []);

    // created numOfMatchesWonPtPyObject object to store the number of matches won per
    // team per year
    /* Ex :- 
          numOfMatchesWonPtPyObject = {
                                        "2008":{
                                                  "Kolkata Knight Riders":6,
                                                  "Chennai Super Kings":9
                                               },
                                        "2009": {
                                                  "Deccan Chargers":9,
                                                  "Chennai Super Kings":8
                                                }
                                      }
    */
    let numOfMatchesWonPtPyObject = yearArr.reduce(function(obj, year) {
      let filteredArr = matches.filter(function(element) {
        return element.season === year;
      });
      // created a team object to store the name of the team as a key and
      // win count as its value
      /*
          Ex:- teamObject = {
                              "Kolkata Knight Riders":6,
                              "Chennai Super Kings":9
                            }
      */
      let teamObject = filteredArr.reduce(function(obj, element) {
        if (element.winner in obj) 
          {
            obj[element.winner]++;
          } 
        else 
          {
            obj[element.winner] = 1;
          }

        return obj;
      }, {});

      obj[year] = teamObject;
      return obj;
    }, {});

    return numOfMatchesWonPtPyObject;
  },

  // extraRunsPerTeam2016 is function to calculate extra runs conceded per team
  // in 2016

  extraRunsPerTeam2016: function(matches, deliveries, year) {

    let filteredTeamArr = matches.reduce(function(arr, element) {
      if (arr.indexOf(element.team1) === -1 && element.season === year) 
        {
          arr.push(element.team1);
        }
      return arr;
    }, []);

    let filteredIdArr = matches.reduce(function(arr, element) {
      if (element.season === year) 
        {
          arr.push(element.id);
        }
      return arr;
    }, []);

    // created an object to store extra runs conceded by each team in 2016
    /* 
        Ex :- {
                "Mumbai Indians":102,
                "Delhi Daredevils":106
              }
    */
    let extraRunsPerTeamObj = filteredTeamArr.reduce(function(obj, team) {
      let extraRunsByTeam = deliveries.reduce(function(run, delivery) {
        if (delivery.bowling_team === team && 
            filteredIdArr.indexOf(delivery.match_id) !== -1) 
          {
            run += delivery.extra_runs;
          }
        return run;
      }, 0);

      obj[team] = extraRunsByTeam;
      return obj;
    }, {});

    return extraRunsPerTeamObj;
  },

  // top10EconomicalBowlers function to calculate top 10 economical bowlers in 2015

  top10EconomicalBowlers: function(matches, deliveries, year) {

    let filteredIdArr = matches.reduce(function(arr, element) {
      if (element.season === year) {
        arr.push(element.id);
      }
      return arr;
    }, []);
    
    let bowlerNamesArr = deliveries.reduce(function(arr, delivery) {
      if (filteredIdArr.indexOf(delivery.match_id) !== -1 && 
          arr.indexOf(delivery.bowler) === -1) 
        {
          arr.push(delivery.bowler);
        }
      return arr;
    }, []);

    // created an object to store the economy of each bowler in 2015
    /*
        Ex :- bowlerEconomyObj = {
                                  "RN ten Doeschate":4,
                                  "J Yadav":4.142857142857142
                                 }
    */

    let bowlerEconomyObj = bowlerNamesArr.reduce(function(obj, name) {

      let numberOfBallsByBowler = 0;
      let bowlerRuns = deliveries.reduce(function(run, delivery) {
        if (
          filteredIdArr.indexOf(delivery.match_id) !== -1 &&
          delivery.bowler === name
        ) {
          run = run +
                      delivery.noball_runs +
                      delivery.batsman_runs +
                      delivery.wide_runs;
          if (delivery.noball_runs === 0 && delivery.wide_runs === 0) {
            numberOfBallsByBowler++;
          }
        }
        return run;
      }, 0);
      
      let bowlerEconomy = (bowlerRuns / numberOfBallsByBowler) * 6;
      obj[name] = bowlerEconomy;
      return obj;
    }, {});
    // created a top10EconomicalBowlers Object to get the top 10 bowlers 
    // starting from minimum economy
    let top10EconomicalBowlersObj = Object.fromEntries(
      Object.entries(bowlerEconomyObj)
        .sort(function(a, b) {
          return a[1] - b[1];
        })
        .slice(0, 10)
    );

    return top10EconomicalBowlersObj;
  },

  // tossWonAndMatchWon function to calculate number of times each team won toss and match too
  /*
      Ex:- tossWonAndMatchWonObj = {
                                      "Kolkata Knight Riders":44,
                                      "Kings XI Punjab":28
                                   }
  */
  
  tossWonAndMatchWon: function(matches) {
    
    let tossWonAndMatchWonObj = matches.reduce(function(obj, element) {
      if (element.toss_winner === element.winner) 
        {
          if (obj.hasOwnProperty(element.winner)) 
            {
              obj[element.winner]++;
            } 
          else 
            { 
              obj[element.winner] = 1;
            }
        }
      return obj;
    }, {});
    return tossWonAndMatchWonObj;
  },

  // playerOfTheSeason function to find player of the season for each season of ipl
  /*
      Ex:- playerOfTheSeasonObj = {
                                      2008":
                                            {
                                              "name":"SE Marsh",
                                              "playerOfTheMatchCount":5
                                            }
                                      "2009":
                                             {
                                              "name":"YK Pathan",
                                              "playerOfTheMatchCount":3
                                             }
                                  }
  */

  playerOfTheSeason: function(matches) {
    
    let yearArr = matches.reduce(function(arr, element) {
      if (arr.indexOf(element.season) === -1) {
        arr.push(element.season);
      }
      return arr;
    }, []);
    
    let playerOfTheSeasonObj = yearArr.reduce(function(obj, year) {
      let playerOfTheMatchCount = matches.reduce(function(obj, element) {
        if (element.season === year) 
          {
            if (obj.hasOwnProperty(element.player_of_match)) 
              {
                obj[element.player_of_match]++;
              } 
            else 
              {
                obj[element.player_of_match] = 1;
              }
          }
        return obj;
      }, {});

      // player of the season will be the one with maximum number of player of match count
      let playerOfTheSeason = Object.keys(playerOfTheMatchCount).reduce(function(a, b) {
          return playerOfTheMatchCount[a] > playerOfTheMatchCount[b] ? a : b;
        },"");
      
      obj[year] = {name: playerOfTheSeason,
        playerOfTheMatchCount: Math.max(...Object.values(playerOfTheMatchCount))
      };
      return obj;
    }, {});

    return playerOfTheSeasonObj;
  },

  // strikeRateOfVirat function to calculate his strike rate every season
  /*
      Ex - strikeRateOfViratObj = {
                                    "2008":105.09554140127389,
                                    "2009":112.32876712328768
                                  }
  */

  strikeRateOfVirat: function(matches, deliveries) {
    
    let yearArr = matches.reduce(function(arr, element) {
      if (arr.indexOf(element.season) === -1) {
        arr.push(element.season);
      }
      return arr;
    }, []);
    
    let strikeRateOfViratObj = yearArr.reduce(function(obj, year) {
      let matchIdArr = matches.reduce(function(arr, element) {
        if (element.season === year) {
          arr.push(element.id);
        }
        return arr;
      }, []);
      
      let numOfBallsFaced = 0;
      let runs = deliveries.reduce(function(run, delivery) {
        if (matchIdArr.indexOf(delivery.match_id) !== -1 &&
            delivery.batsman === "V Kohli") 
            {
                run = run + delivery.batsman_runs;
                if (delivery.noball_runs === 0 && delivery.wide_runs === 0) 
                  {
                    numOfBallsFaced++;
                  }
            }
        return run;
      }, 0);
      
      let strikeRate = (runs / numOfBallsFaced) * 100;
      obj[year] = strikeRate;
      return obj;
    }, {});
    return strikeRateOfViratObj;
  },

  // economyOfBowlersInSuperOvers function to find the best economy in super overs
  /*
      Ex :- economyOfBowlersInSuperOversObj = {
                                                "JJ Bumrah":4,
                                                "Kamran Khan":15
                                              }
  */
  economyOfBowlersInSuperOvers: function(deliveries) {
  
    let superOverArr = deliveries.filter(function(element) {
        return element.is_super_over !== 0;
      }
    );
    
    let bowlerNamesArr = superOverArr.reduce(function(arr, element) {
      if (arr.indexOf(element.bowler) === -1) {
        arr.push(element.bowler);
      }
      return arr;
    }, []);
    // created the best economy object to store the economy values 
    // in their respective bowler name key
   
    let bestEconomyObj = bowlerNamesArr.reduce(function(obj, element) {
      let superOverWithBowlerName = superOverArr.filter(function(arrElement) {
        return arrElement.bowler === element;
      });
     
      let numOfBalls = 0;
      let bowlerRuns = superOverWithBowlerName.reduce(function(run, ball) {
        run = run +
                    ball.noball_runs +
                    ball.batsman_runs +
                    ball.wide_runs;
        if (ball.noball_runs === 0 && ball.wide_runs === 0) {
          numOfBalls++;
        }
        return run;
      }, 0);

      let bowlerEconomy = (bowlerRuns / numOfBalls) * 6;
      obj[element] = bowlerEconomy;
      return obj;
    }, {});

    return bestEconomyObj;
  },

  // playerDismissal function to find the highest number of times one player 
  // has been dismissed by another player
  /*
      Ex:- playerDismissalObj = {
                                  "DA Warner":
                                              {
                                                "bowlerName":"UT Yadav",
                                                "dismissalCount":3
                                              }
                                }
  */
  playerDismissal: function(deliveries) {
   
    let namesOfPlayerDismissed = deliveries.reduce(function(arr, element) {
      if (arr.indexOf(element.player_dismissed) === -1 &&
          element.player_dismissed !== "") 
          {
            arr.push(element.player_dismissed);
          }
      return arr;
    }, []);

    // created a player dismissal count object to store the player name as its key 
    // and the value will be the bowler with max dismissals and its count
    let playerDismissalCountObj = namesOfPlayerDismissed.reduce(function(obj,element) {
      let bowlerThatDismissedBatsmanObj = deliveries.reduce(function(newObj, delivery) {
        if (delivery.player_dismissed === element) {
          if (newObj.hasOwnProperty(delivery.bowler) &&
              delivery.dismissal_kind !== "run out" &&
              delivery.dismissal_kind !== "") 
               {
                  newObj[delivery.bowler]++;
               } 
          else {
                  newObj[delivery.bowler] = 1; 
               }
        }
          return newObj;
      },{});
      
      let bowlerNameWithMaxDismissal = Object.keys(bowlerThatDismissedBatsmanObj).reduce(function(a, b) {
        return bowlerThatDismissedBatsmanObj[a] > bowlerThatDismissedBatsmanObj[b] ? a : b;
        }, "");
      
      let maxNumberOfDismissals = Object.values(bowlerThatDismissedBatsmanObj).sort((prev, next) => next - prev)[0];
      obj[element] = {
                        bowlerName: bowlerNameWithMaxDismissal,
                        dismissalCount: maxNumberOfDismissals
                     };
      return obj;
    },{});
    return playerDismissalCountObj;
  }
};
