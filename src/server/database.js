// using dotenv module to load environment variables
require('dotenv').config()
// creating a pool to interact with postgres database
const { Pool } = require("pg");
// loading fast-csv modules to parse csv
var csv = require('fast-csv');

const pool = new Pool();

pool.connect(function(err){
  if(err)
  {
      console.log(`Error occurred while connecting \n ${err}`);
  }
});

// matchesCsvStream to stream/read through the data inside matches csv
// {headers: true } means treat the 1st line of csv as headers
let matchesCsvStream = csv.parseFile("../data/matches.csv", { headers: true }).on("data", function(record) {
  // pause after reading the first line of data
  // each line of data is called as record
  matchesCsvStream.pause();
  // storing the data in each line in a variable
  let id = record.id;
  let season = record.season;
  let city = record.city;
  let date = record.date;
  let team1 = record.team1;
  let team2 = record.team2;
  let toss_winner = record.toss_winner;
  let toss_decision = record.toss_decision;
  let result = record.result;
  let dl_applied = record.dl_applied;
  let winner = record.winner;
  let win_by_runs = record.win_by_runs;
  let win_by_wickets = record.win_by_wickets;
  let player_of_match = record.player_of_match;
  let venue = record.venue;
  let umpire1 = record.umpire1;
  let umpire2 = record.umpire2;
  let umpire3 = record.umpire3;
  // calling a query to store the data in their respective column ids inside the
  // matches table with error handling 
  pool.query("INSERT INTO matches(id,season,city,date,team1,team2,toss_winner,toss_decision,result,dl_applied,winner,win_by_runs,win_by_wickets,player_of_match,venue,umpire1,umpire2,umpire3) \ VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18)", 
  [id,season,city,date,team1,team2,toss_winner,toss_decision,result,dl_applied,winner,win_by_runs,win_by_wickets,player_of_match,venue,umpire1,umpire2,umpire3], function(err){
    if(err)
    {
      console.log(` Error occurred while inserting values inside the table \n${err}`);
    }
});
// resume/ continue reading the csv after inserting the data in that line in the table  
matchesCsvStream.resume();
}).on("end", function(){
  console.log("Matches table created using csv");
});

// deliveriesCsvStream to stream/read through the data inside deliveries csv
// {headers: true } means treat the 1st line of csv as headers

let deliveriesCsvStream = csv.parseFile("../data/deliveries.csv", { headers: true }).on("data", function(record) {
  // pause after reading the first line of data
  // each line of data is called as record
  deliveriesCsvStream.pause();
  // storing the data in each line in a variable
  let match_id = record.match_id;
  let inning = record.inning;
  let batting_team = record.batting_team;
  let bowling_team = record.bowling_team;
  let over = record.over;
  let ball = record.ball;
  let batsman = record.batsman;
  let non_striker = record.non_striker;
  let bowler = record.bowler;
  let is_super_over = record.is_super_over;
  let wide_runs = record.wide_runs;
  let bye_runs = record.bye_runs;
  let legbye_runs = record.legbye_runs;
  let noball_runs = record.noball_runs;
  let penalty_runs = record.penalty_runs;
  let batsman_runs = record.batsman_runs;
  let extra_runs = record.extra_runs;
  let total_runs = record.total_runs;
  let player_dismissed = record.player_dismissed;
  let dismissal_kind = record.dismissal_kind;
  let fielder = record.fielder;
  // calling a query to store the data in their respective column ids inside the
  // deliveries table with error handling
  pool.query("INSERT INTO deliveries(match_id,inning,batting_team,bowling_team,over,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder) \ VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21)", 
  [match_id,inning,batting_team,bowling_team,over,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder], function(err){
    if(err)
    {
      console.log(err);
    }
});
// resume/ continue reading the csv after inserting the data in that line in the table  
deliveriesCsvStream.resume();
}).on("end", function(){
  console.log("Deliveries table created using csv");
});

